;;; init.el self-installing, for Emacs 25 and up.

(org-babel-load-file (expand-file-name "~/.emacs.d/settings.org"
				       user-emacs-directory))

;;; init.el ends here
